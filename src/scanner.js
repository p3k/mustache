// 扫描器类
export default class Scanner {
  constructor(str) {
    this.str = str
    // 指针
    this.pos = 0;
    // 尾巴
    this.tail = str
  }

  // 走过指定内容,没有返回值
  scan(stopTag) {
    if (this.tail.indexOf(stopTag) == 0) {
      // 指针加上 stopTag 的长度
      this.pos += stopTag.length
      // 返回指针后剩余内容
      this.tail = this.str.substring(this.pos)
    }
  }

  // 让指针进行扫描,直到遇见指定内容结束,并且能够返回结束之前路过的文字
  scanUtil(stopTag) {
    // 记录执行本方法的pos值
    const posBackup = this.pos
    // 当尾巴的开头不是stopTag的时候,说明还没有扫描到stopTag
    while (!this.eos() && this.tail.indexOf(stopTag) != 0) {
      this.pos++;
      this.tail = this.str.substring(this.pos)
      // console.log(this.pos, this.tail);
    }
    // 返回指针后面剩余内容
    return this.str.substring(posBackup, this.pos)
  }

  // 指针是否已经到头,返回 Boolean 值
  eos() {
    return this.pos >= this.str.length
  }
}