/**
 * 
 * @param {数组数据} tokens 
 * @param {变量} data 
 * @returns 
 */

import lookup from './lookup.js'

export default function renderTemplate(tokens, data) {
  let templateStr = ''

  tokens.forEach((x, i) => {
    if (x[0] == 'text') {
      templateStr += x[1]
    } else if (x[0] == 'name') {
      templateStr += lookup(data, x[1])
    }
  });
  return templateStr;
}