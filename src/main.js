import parseTemplateTokens from './parseTemplateTokens.js'
import renderTemplate from './renderTemplate.js'

window.myTemplate = {
  render(str, data) {
    let tokens = parseTemplateTokens(str)
    
    let renderStr = renderTemplate(tokens, data)
    console.log(renderStr);
  }
}