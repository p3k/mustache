const path = require('path')

module.exports = {
  // "production" | "development" | "none"  // 告诉webpack是生产环境还是开发环境
  mode: "development", 
  // 入口 string | object | array  // 默认 ./src
  entry: "./src/main.js",
  output: {
    // 输出，只可指定一个输出配置
    path: path.resolve(__dirname, "dist"), // string
    // 所有输出文件所在的目录
    // 必须是绝对路径(use the Node.js path module)
    // 输出文件的名称
    filename: "bundle.js", 
    // 相对于HTML页面解析的输出目录的url
    publicPath: "/assets/", 
    // 导出库的名称
    libraryTarget: "umd", // universal module definition    // the type of the 
  },
  devServer: {
    contentBase: path.join(__dirname, 'www'),
    compress: false,
    port: 8080,
    publicPath: '/xuni/'
  }
}
 