import Scanner from './scanner.js'
import nestTokens from './nestTokens.js'

export default function parseTemplateTokens(str) {
  let tokens = [];
  let words
  const scanner = new Scanner(str)
 
  while (!scanner.eos()) {
    // 查找 {{
    words = scanner.scanUtil('{{')
    
    if (words != '') {
      tokens.push(['text', words])
    }
    // 改变指针位置
    scanner.scan('{{')

    // 查找 }}
    words = scanner.scanUtil('}}')
    if (words != '') {
      if (words[0] == '#') {
        tokens.push(['#', words.substring(1)])
      } else if (words[0] == '/') {
        tokens.push(['/', words.substring(1)])
      } else {
        tokens.push(['name', words])
      }
    }
    // 返回指针位置
    scanner.scan('}}')
  }
  return nestTokens(tokens);
}