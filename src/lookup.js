/**
 * @param {复杂数据} obj 
 * @param {需要查找的复杂类型值} keyName 
 * 例如obj :
 *   {
 *      a: {
 *        b: {
 *          c: 100
 *        }
 *      }
 *    }
 *  查找的keyName: a.b.c
 */

export default function lookup(obj, keyName) {
  if (keyName.indexOf('.' != -1)) {
    let keys = keyName.split('.')
    let step = obj
    keys.forEach(x => {
      step = step[x]
    });
    return step
  }
  return obj[keyName]
}