export default function nestTokens(tokens) {
  let nestTokens = []
  let temporary = nestTokens
  let sections = []
  for (let i = 0; i < tokens.length; i++) {
    let x = tokens[i]
    switch(x[0]) {
      case '#':
        temporary.push(x)
        sections.push(x)
        temporary = x[2] = []
        break;
      case '/':
        sections.pop()
        temporary = sections.length > 0 ? sections[sections.length - 1][2] : nestTokens
        break;
      default:
        temporary.push(x)
    }
  }
  return nestTokens;
}